jQuery(document).ready(function($){
	/* Initialization of input elements and ImageUploader.js */
	$("input.image-upload").each(function(index){
		var teamid=$(this).attr('data-teamid');
		var code=$(this).attr('data-code');
		var uploader = new ImageUploader({
      'inputElement': $(this).get(0),
			'onProgress': function(info)
			{
				/* Updating the progress bar */
				if (info['currentItemTotal']<=0)
					return;
				var progress=info['currentItemDone']*100.0/info['currentItemTotal'];
				console.log("progress ",progress);
				$('#uploading-msg').text("pujant "+Math.floor(progress)+'%');
				$('#upload-progress'+' div').css('width',progress+'%');

			},
			'onComplete': function()
			{
				/* Enable upload button */
			//	$('#upload-button').removeProp('disabled');
				/* Hide progress bar */
			//	$("#upload-container").addClass("hidden");

				$("#upload-container").addClass("hidden");
				showMsg("Foto pujada!. Si vols pots pujar més imatges")
			//	$('img').css({width:'32%',float:'left'});
			},
			/* Add rand parameter to prevent accidental caching of the image by the server */
			'uploadUrl': base_url+'main/sendImage/?action=upload_image&teamid=' + teamid + '&code='+code+'&rand=' + new Date().getTime(),
			'debug': true,
      'maxSize':1.0 //mida en megapixels  , sembla que no funciona?
			});
	});

	/* The function below is triggered every time the user selects a file */
	$("input.image-upload").change(function(index){

		/* We will check additionally the extension of the image if it's correct and we support it */
		var extension = $(this).val();
		if (extension.length>0){
			extension = extension.match(/[^.]+$/).pop().toLowerCase();
			extension = ~$.inArray(extension, ['jpg', 'jpeg']);
		}
		else{
			event.preventDefault();
			return;
		}

		if (!extension)
		{
			event.preventDefault();
			console.error('Unsupported image format');
			return;
		}

		/* Disable upload button until current upload completes */
		$('#upload-button').prop('disabled',true);
		/* Show progress bar */
		$("#upload-container").removeClass("hidden");
		/* If you want, you can show a preview of the selected image to the user, but to keep the code simple, we will skip this step */
	});
});
