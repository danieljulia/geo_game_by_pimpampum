<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link type="text/css" rel="stylesheet" href="<?php print base_url()?>/assets/css/admin.css" />
		<script
		  src="https://code.jquery.com/jquery-2.2.4.min.js"
		  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
		  crossorigin="anonymous"></script>


<?php
if(isset($css_files)):
foreach($css_files as $file): ?>
	<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>

<?php foreach($js_files as $file): ?>
	<script src="<?php echo $file; ?>"></script>
<?php endforeach;
else:
?>

<?php
	?>

	<?php
endif;?>


<script src="<?php print base_url()?>/assets/js/admin.js"></script>
<script src="<?php print base_url()?>/assets/js/handlebars-v4.0.5.js"></script>

</head>
<body>
	<ul id="menu">
	    <li><a href='<?php echo site_url('admin/cache')?>'>Caches</a></li>
		<li><a href='<?php echo site_url('admin/team')?>'>Equips</a></li>
		<li><a href='<?php echo site_url('admin/points')?>'>Punts</a></li>
		<li><a href='<?php echo site_url('admin/images')?>'>Imatges</a></li>
		<li><a href='<?php echo site_url('admin/tracking')?>'>Tracking</a></li>
	</ul>
