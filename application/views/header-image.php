<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />



    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.2/css/uikit.gradient.min.css" integrity="sha256-TLgWPS4CoQk94wbnfeHKRaBpM3IQS42Y3pqb3CTDFJI=" crossorigin="anonymous" />
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.2/css/components/progress.gradient.min.css" integrity="sha256-z+JFPUwyB++qqAA/9qdfL/hpFnwdDWTAq5gYt0+OkZE=" crossorigin="anonymous" />
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.2/css/components/form-file.gradient.min.css" integrity="sha256-E4mr2pSGSE3pbt5V6fEn+e2mA2xqfagllq4tRqJUsOk=" crossorigin="anonymous" />

  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
  	<script type="text/javascript" src="<?php print base_url()?>js/exif.js"></script>
  	<script type="text/javascript" src="<?php print base_url()?>js/ImageUploader.js"></script>
  	<script type="text/javascript" src="<?php print base_url()?>js/custom.js"></script>

    <link rel="stylesheet" href="<?php print base_url()?>css/style.css" />
    <script
      src="https://code.jquery.com/jquery-2.2.4.min.js"
      integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
      crossorigin="anonymous"></script>

  </head>
<?php require "subheader.php" ?>
