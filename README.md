# geo_game_by_pimpampum

# Simple Geocaching game based in LAMP

This is a simple geocaching game based in
- CodeIgniter
- Grocery CRUD
- Leaflet js and OpenStreetmap
- HTML5 geolocation
- ImageUploader.js (js library to resize and upload images)

Made by http://pimpampum.net for the escape.cat 2018 festival

#requirements

- LAMP
- https server (important in order to html5 geolocation to work)

# Installation

- Note: All texts are in catalan right now
- Create database with install/geogame.sql dump
- Copy all files to the server
- Rename application/config/database.sample.php to
database.php and add the db settings
'hostname' => '',
'username' => '',
'password' => '',
'database' => '',
- Create a directory named uploads with write permission
- Change the RewriteBase line in .htaccess (or remove if in root web folder)
- Change base_url in application/config.php
$config['base_url'] = 'https://www.pimpampum.info/esc/';



- You can protect the admin tool (/admin) creating a admin folder and adding
a .htaccess file:
Change route in admin/.htaccess
AuthUserFile /full path to /.htpasswd
Add a .htpasswd file


# Instructions

The game is in /

And the backoffice in /admin

The game has 2 phases

The first one to hide the caches. Each cache has a 5 letter code.
You have to generate the code for all the caches in the admin tool (adding caches)
To activate this game phase set the mode to REC in the application/config/esc.php configuration file
Then people is able to play the game hiding the caches, they won't see anything still on the map.

To activate the second phase set  the mode to VIEW. The caches for the other teams will be visible.

Teams can get points from saving a cache, catching one in the second phase of the game, and loose point reading the spoiler text or the image. The points can be changed in the configuration file.

Teams need to register first, but they don't need a pasword. There is no a real authentication
(in order to simplify the game)


# Improvement suggestions (maybe in the next version)

- Make it translatable (currenly only in catalan)
- Disable continue button while uploading an image
- Show how many teams have reached a cache (in the cache bubble)
- Cache points depending on the distance to the "base camp"
- Disable to catch your own codes (not visible on the map but still working)
- Show more info in the ranking section
- Add a global message visible in the app (that can be changed in the admin section)

- Real authentication


- Improve the visualization /visualization
