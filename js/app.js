//geolocation
//bon exemple https://www.arkaitzgarro.com/html5/capitulo-11.html

var is_geolocated=false;
var lat, lng;
var distancia=0;

function geo_success( position) {
  console.log("position",position);
  setCursor(position);

}

function geo_error(err) {
  console.log("error",err);
  $('#geo').text("error");
}

//https://developer.mozilla.org/en-US/docs/Web/API/PositionOptions
var geo_options = {
  enableHighAccuracy: true,
  maximumAge        : 1000 //temps de cache en ms
  //timeout           : 0
};

function geolocate(){

  navigator.geolocation.watchPosition(geo_success, geo_error, geo_options);
}




/** final geolocalització */

var team="";
var data;
var notFoundIcon,foundIcon;
var marker_selected;
var marker_line;

var base = {
  //'Empty': L.tileLayer(''),
  'OpenStreetMap': L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    'attribution': 'Map data &copy; OpenStreetMap contributors'
  })
};



if(document.getElementById("map")){ //if map exists...
  geolocate();
  console.log("el mapa existeix...");
  //fundació La Plana
  //41.848252,2.011046
  var map = L.map('map', {
    'center': [41.848252,2.011046],
    'zoom': 16,
    'layers': [
      base.OpenStreetMap
    ],
    maxZoom: 18,
    minZoom: 12
  });

  ///var control = L.control.layers(base).addTo(map);
  var marker_created=false;
  var current_circle;
  var radius=-1;


  function setCursor(position){
    console.log("situant el marcador");
    lat=position.coords.latitude;
    lng=position.coords.longitude;


    var pos={lat:lat,lng:lng};
    radius = position.coords.accuracy; // /2?

    $('input[name="lat"]').val(lat);
    $('input[name="lng"]').val(lng);
    $('input[name="precision"]').val(radius);
    is_geolocated=true;
    $('#geo').text("ok "+position.coords.accuracy);


    if(!marker_created){
      current_circle=  L.circle(pos, radius);
      current_circle.addTo(map);
      marker_created=true;


      marker_line = L.polyline([[lat,lng],[lat,lng]], {color: 'red'}).bindPopup('Hi There!').addTo(map);
      //map.panTo(new L.LatLng(pos.lat, pos.lng));
      map.setView([lat, lng],16);

    }else{
      current_circle.setLatLng(pos);
      current_circle.setRadius(radius);

      if(marker_selected){

        var latlngs = [
            [lat, lng],
            [parseFloat(marker_selected.data.lat),
              parseFloat(marker_selected.data.lng) ]
        ];
        //console.log("posicions",latlngs);
        marker_line.setLatLngs(latlngs);

        var latlng = L.latLng(lat, lng);

        distancia=parseInt(latlng.distanceTo([parseFloat(marker_selected.data.lat),parseFloat(marker_selected.data.lng) ]));
          //console.log("distance ",d);
          $('.marker-distance').text("("+distancia+"m)");

      }
    }
  //  $('.info').text("Precisió: "+radius+"m");

    //tracking

    //todo, verificar que hi ha nom d'equip

      console.log("fent tracking");
      //todo, limitar les crides (per ex si no ha canviat ubicació, per limitar transit i base de dades)
      $.getJSON(base_url+"api/track/"+team_id+"/"+lat+"/"+lng+"/"+parseInt(radius),function(data){
          console.log("tracking ",team_id);
      });
  }//en setcursor

  var showPopup = function(e) {
    content=e.target.data.name;
    popup = L.popup({ className : 'custom-popup' }).setContent(content).setLatLng(e.latLng);

    popup.openOn(map);


  }


  console.log("buscant caches");
  notFoundIcon = L.icon({
    iconUrl: base_url+'img/marker2_on.png',
    //shadowUrl: 'leaf-shadow.png',

    iconSize:     [30, 30], // size of the icon
    shadowSize:   [30, 30], // size of the shadow
    iconAnchor:   [15, 15], // point of the icon which will correspond to marker's location
    shadowAnchor: [15, 15],  // the same for the shadow
    popupAnchor:  [0, -36] // point from which the popup should open relative to the iconAnchor
});

foundIcon = L.icon({
  iconUrl: base_url+'img/marker2_off.png',
  //shadowUrl: 'leaf-shadow.png',

  iconSize:     [22, 22], // size of the icon
  shadowSize:   [22, 22], // size of the shadow
  iconAnchor:   [11, 11], // point of the icon which will correspond to marker's location
  shadowAnchor: [11, 11],  // the same for the shadow
  popupAnchor:  [0, -36] // point from which the popup should open relative to the iconAnchor
});


  console.log("demanant caches");
  $.getJSON(base_url+"api/caches/"+team_id,function(indata){
    data=indata;
    console.log("caches",mode,data);
    if(mode=="REC"){
      //no ensenyar els marcadors, només el nombre que has guardat

        //$('p.points').text(data.length+" cachés guardats");



    }else{

      for(var i=0;i<data.caches.length;i++){
        var d=data.caches[i];


        var icon;
        if( data.found.indexOf(d.code)!=-1){
          icon=foundIcon;
        }else{
          icon=notFoundIcon;
        }

        var marker=L.marker([d.lat, d.lng], {icon: icon}).on('click', onClick);
        marker.data=d;
        marker.addTo(map)
        var html="<div class='bubble' data-code='"+d.code+"'><h2>"+d.name+"</h2><span class='marker-distance'>distància</span>";
        html+="<ul><li><a class='msg' href='#'>Veure missatge</a> (-30 punts)</li>";
        if(d.images!="0") html+="<li><a class='images' href='#'>Veure imatges</a> (-30 punts) </li></ul>";
        html+="</div>";
        marker.bindPopup(html);

      }
    }

    /*

    $('.bubble a').on('click',function(ev){
        console.log(ev);
        console.log("soc aqui...hehe",$(this));
        console.log($(this).data('code'));
    });*/
  });

}


/*
function onLocationFound(e) {
  console.log("he trobat la info ",e);
    var radius = e.accuracy / 2;

    if(!marker_created){
      current_marker=L.marker(e.latlng);
      current_circle=  L.circle(e.latlng, radius);

      current_marker.addTo(map)
        .bindPopup("You are within " + radius + " meters from this point").openPopup();

      current_circle.addTo(map);
        marker_created=true;
    }
}
*/

function getMessage(code){
  for(var i=0;i<data.caches.length;i++){
    if(data.caches[i].code==code){
      return data.caches[i].msg;
    }
  }
}
function onClick(e) {
  marker_selected=e.target;
  var latlngs = [
      [lat, lng],
      [e.latlng.lat,
        e.latlng.lng ]
  ];


  //map.addLayer(marker_line);//For show
  //map.removeLayer(marker_line);// For hide

  setTimeout(function(){
    $('.bubble a.msg').on('click',function(ev){
        //registrar punts menys
        var code=$(this).parents('.bubble').data('code');
        var msg=getMessage(code);
        $('.bubble .msg').parent().html(msg);
        $.getJSON(base_url+"api/spoiler_seen/"+team_id+"/"+code,function(data){
          console.log("punts d'spoiler restats",data);
          var p=  parseInt( $('.counter').text() );
          $('.counter').text(p+data);
        });

    });

    $('.bubble a.images').on('click',function(ev){
        //registrar punts menys
          var code=$(this).parents('.bubble').data('code');
        $.getJSON(base_url+"api/image_seen/"+team_id+"/"+code,function(data){
          console.log("punts d'imatge restats",data);
          var p=  parseInt( $('.counter').text() );
          $('.counter').text(p+data);
        });

        var code=$(this).parents('.bubble').data('code');
        $.getJSON(base_url+"api/images/"+code,function(data){
          if(data.length>0){
            console.log("images ",data);
            var html="";
            for(var i=0;i<data.length;i++){
              html+="<img src='"+base_url+"/uploads/"+data[i].file+"'>";
            }

            $('#images .slider').html(html);
            $('#images').removeClass("hidden");
            initSlider();

          }
        });



    });

    var latlng = L.latLng(lat, lng);

        marker_line.setLatLngs(latlngs);

     distancia=parseInt(latlng.distanceTo([e.latlng.lat,e.latlng.lng ]));
      console.log("distance ",distancia);
      $('.marker-distance').text("("+distancia+"m)");


  },500);

}


$(document).ready(function(){

  $('#images .close').on('click',function(ev){
    $('#images').addClass("hidden");

    if($('.slick-initialized').length>0){
      $('.slider').slick('unslick');
      $('.slider').html('');
    }

  });


  $('.deixar').on('click',function(ev){
    //esborrar
      if(radius=="-1"){
        alert("no hi ha prou precisió per deixar l'objecte");
        return;
      }

    //  $('#myform').submit();
      //ev.preventDefault();

  });

    $('input[name="team"]').on('input',function(ev){
      team=$(this).val();
      localStorage.setItem("team", team);

    });

    // Toggles

      var $burgers = getAll('.burger');

      if ($burgers.length > 0) {
        $burgers.forEach(function ($el) {
          $el.addEventListener('click', function () {

            var target = $el.dataset.target;
            var $target = document.getElementById(target);
            $el.classList.toggle('is-active');
            $target.classList.toggle('is-active');
          });
        });
      }





});


function initSlider(){



  $('.slider').slick({
    dots: true,
    infinite: true,
    speed: 500,
    slide:'img',
    slidesToShow: 1,
    slidesToScroll: 1

  });
}


function showMsg(msg){
  $('#form-msg').text(msg);
    $('#form-msg').removeClass('hidden');
}


// Functions

function getAll(selector) {
  return Array.prototype.slice.call(document.querySelectorAll(selector), 0);
}
