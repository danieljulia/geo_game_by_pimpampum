<?php


class Cache  extends CI_Model  {


	function __construct()
    {
        parent::__construct();
    }

    //saves a cache in a location by a team
  function save($code,$lat,$lng,$prec,$owner_id,$message=""){
    //todo only works if in save mode
    $data = array(
            'prec' => $prec,
            'lat' => $lat,
            'lng' => $lng,
            'msg'=>$message,
            'owner' => $owner_id
    );

    $this->db->where('code', $code);
    $this->db->update('cache', $data);
  }

  function is_saved($code){
    $query = $this->db->get_where('cache', array('code' => $code));
    $row = $query->row();


    if (isset($row))
    {
      if($row->owner=="0") return false;
      else return true;
    }

  }

	function get_cache($code){
		$query = $this -> db
							 -> select('*')
							 -> where('LOWER(code)', strtolower($code))
							 -> get('cache');

	 	return $query->row();

	}



		function is_valid($code){
	    $query = $this -> db
	               -> select('*')
	               -> where('LOWER(code)', strtolower($code))
	               -> get('cache');

	   if ( $query->num_rows() > 0 )
	   {
	       return true;
	   }else{
	     return false;
	   }

		}


  //sets an image to a cache
  function set_image($code,$file,$team_id,$type="spoiler"){
		$data = array(
		        'team_id' => $team_id,
						'file'=>$file,
						'type'=>$type,
						'cache'=>$code

		);
		$this->db->insert('images', $data);
		$insert_id = $this->db->insert_id();

		$this->db->where('code', $code);
		$this->db->set('images', 'images+1', FALSE);
		$this->db->update('cache');

   	return  $insert_id;
  }

	function get_total(){

			$sql="select count(*) as t from cache where owner<>0";
			$query=$this->db->query($sql);
			return $query->row()->t;
		}

  //return all caches for a team
  function get_all($teamid=0){

		if($this->config->item("mode")=="REC"){

			/*
			$query = $this -> db
								 -> select('*')
								 -> where('owner = '.$teamid, null,false)
								 -> get('cache');*/
			 $sql="select * from cache,team where cache.owner=team.id and owner=?";
			 $query=$this->db->query($sql,array($teamid));

		}else{
			/*
			$query = $this -> db
								 -> select('*')
								 -> where('owner != '.$teamid, null,false)
								 -> where('owner !=0', null,false)
								 -> get('cache');*/
		 $sql="select * from cache,team where cache.owner=team.id and owner!=?";
			$query=$this->db->query($sql,array($teamid));
		}

		return $query->result();

  }

	function get_found($teamid){
		$sql="select cache from points where type='find' and team_id=?";
	  $query=$this->db->query($sql,array($teamid));
		$res=array();
		foreach ($query->result() as $row)
		{

						$res[]=$row->cache;
		}
	  return $res;
	}

}
