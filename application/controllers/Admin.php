<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	public $crud;

	public function __construct()
	{
		parent::__construct();



		$this->load->database();
		$this->load->library('grocery_CRUD');
		$this->crud= new grocery_CRUD();
		$this->crud->unset_export();
		$this->crud->unset_print();
	}

	public function _show_output($output = null)
	{
		$this->load->view('admin/admin.php',(array)$output);
	}

	function index(){
		$this->cache();
	}

	function cache(){

			$this->crud->set_theme('bootstrap-v4'); //datatables
			$this->crud->set_table('cache')->columns('code','owner','lat','lng'); //els camps que ensenya al llistat
				$this->crud->set_relation('owner','team','name');
			$this->crud->set_subject('Catxés');
			$this->crud->callback_add_field('code',array($this,'add_field_codi'));
			$output = $this->crud->render();
			$this->_show_output($output);
	}

	function add_field_codi()
	{
		return '<input type="text" maxlength="50" value="'.$this->_createCode().'" name="code" style="width:462px">';
	}



	function team(){

			$this->crud->set_theme('bootstrap-v4'); //datatables
			$this->crud->set_table('team');

			$this->crud->set_subject('Equips');
			$output = $this->crud->render();
			$this->_show_output($output);
	}

	function points(){

			$this->crud->set_theme('bootstrap-v4'); //datatables
			$this->crud->set_table('points');
			$this->crud->set_relation('team_id','team','name');
			$this->crud->set_subject('Punts');
			$output = $this->crud->render();
			$this->_show_output($output);
	}

	function tracking(){

			$this->crud->set_theme('bootstrap-v4'); //datatables
			$this->crud->set_table('tracking');
				$this->crud->set_relation('team_id','team','name');
			$this->crud->set_subject('Tracking');
			$output = $this->crud->render();
			$this->_show_output($output);
	}

	function images(){

			$this->crud->set_theme('bootstrap-v4'); //datatables
			$this->crud->set_table('images');
				$this->crud->set_relation('team_id','team','name');
			$this->crud->set_subject('Imatges');
						$this->crud->callback_column('file',array($this,'add_field_image'));
			$output = $this->crud->render();
			$this->_show_output($output);
	}

	function add_field_image($value, $row)
	{

		return '<img width="100" src="'.site_url().'/uploads/'.$value.'">';
	}


	function _createCode(){
		$seed = str_split(''
                 .'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
                 .''); // and any other characters
			shuffle($seed); // probably optional since array_is randomized; this may be redundant
			$rand = '';
			foreach (array_rand($seed, 5) as $k) $rand .= $seed[$k];
			return $rand;

	}


	function nodes(){


			$this->crud->set_theme('bootstrap-v4'); //datatables
			$this->crud->set_table('nodes')->columns('tree_id','title','edge','parent_id');
			$this->crud->set_relation('parent_id','nodes','title');
			$this->crud->set_relation('tree_id','trees','name');
			$this->crud->fields('tree_id','title','edge','parent_id');
			$this->crud->display_as('edge','Subtitle');
			$this->crud->display_as('tree_id','Tree');
			$this->crud->display_as('parent_id','Parent node');

			$this->crud->set_subject('Nodes');

			$output = $this->crud->render();

			$this->_show_output($output);

	}



	//todo should be a model
	function terms_get(){
		$query = $this->db->query('select *,a.id as term_id,a.name as term_name from iframe_term as a,iframe_tax as b where a.id_tax=b.id order by b.name,a.order  ');
		return $query->result();
	}
	function doc_get(){
		$query = $this->db->query('select * from iframe_doc as a LEFT JOIN iframe_doc_2_term as b on  a.id=b.id_doc order by a.id ');

		return $query->result();
	}

	function create_file($tree_id){

		$nodes=$this->nodes_get($tree_id);

		$root=$this->tree_get_root($nodes);
		if(count($root)!=1){
			print "only a root node is mandatory...";
			return;
		}

		$init=array();
		$node = new StdClass;
		$node->name=$root[0]->title;
		$node->subname=$root[0]->edge;
		$node->parent_id=$root[0]->parent_id;
		$node->id=$root[0]->id;

		$this->tree_create($nodes,$node);

		$filename='../data/decision_tree_data_'.$tree_id.'.json';
		$fp = fopen($filename, 'w');
		fwrite($fp, json_encode($node));
		fclose($fp);

		echo $filename." json file created";


	}

	function create(){
		$trees=$this->trees_get();


			//$this->_show_output($output); //array('trees'=>$trees)
				//$this->load->view('admin/admin.php',(array)$output);
    $this->load->view('admin/trees',array('trees'=>$trees));
	}

	function tree_create($nodes,&$node){

		$node->children=$this->tree_get_children($nodes,$node);


		if(count($node->children)==0){

			return $node;
		}

		foreach($node->children as $child){

			$this->tree_create($nodes,$child);
		}

	}

	function tree_get_root($nodes){
		$children=array();
		foreach($nodes as $node){
			if($node->parent_id==0){
				$children[]=$node;
			}
		}
		return $children;

	}


	function tree_get_children($nodes,$innode){

		$children=array();
		foreach($nodes as $node){
			if($node->parent_id==$innode->id){
				$n=new stdClass();
				$n->id=$node->id;
				$n->name=$node->title;
				$n->subname=$node->edge;
				$n->parent_id=$node->parent_id;
				$children[]=$n;
			}
		}
		return $children;

	}

	function trees_get(){
		$query = $this->db->query('select * from trees');

		return $query->result();
	}

	function nodes_get($tree_id){
		$query = $this->db->query('select * from nodes where tree_id=?',$tree_id);

		return $query->result();
	}


}
