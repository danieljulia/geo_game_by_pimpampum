<?php require "header.php" ?>
<div class="content create">
  <h2>Create JSON file</h2>

  <ul>
  <?php foreach($trees as $tree):?>
<li><a href="<?php echo site_url('admin/create_file/'.$tree->id)?>"><?php echo $tree->name?></a></li>
  <?php endforeach ?>
</ul>
</div>
<?php require "footer.php"?>
