<?php


class Team_model  extends CI_Model  {


	function __construct()
    {
        parent::__construct();

    }

  function register($name){
		if( $this->team_exists($name) ){
			return -1;
		}

		$data = array(
		        'name' => strtoupper($name),
		);
		$this->db->insert('team', $data);
		$insert_id = $this->db->insert_id();

   	return  $insert_id;

  }

	function team_exists($name){
		$query = $this -> db
							 -> select('*')
							 -> where('name', strtoupper($name))
							 -> get('team');
		 $res=$query->result();
		 if(count($res)>0) return true;
		 return false;
	}

	function team_id_exists($id){
		$query = $this -> db
							 -> select('*')
							 -> where('id', $id)
							 -> get('team');
		 $res=$query->result();
		 if(count($res)>0) return true;
		 return false;
	}

	function cache_find($teamid,$code){
			//veriticar que no és una segona vegada
			if($this->cache_found($teamid,$code,"find")){
				return "Ja has trobat aquest objecte";

			}

			$this->points_set($teamid,$code,'find',$this->config->item("points_cache"));
			return "Molt bé, has obtingut ".$this->config->item("points_cache")." punts";

	}

	function cache_saved($code){
		$query = $this -> db
							 -> select('*')
							 -> where('cache', $code)
							 -> where('type', 'save')
							 -> get('points');
		 $res=$query->result();
		 if(count($res)>0) return true;
		 return false;

	}


	function cache_found($teamid,$code,$type="find"){
		$query = $this -> db
							 -> select('*')
							 -> where('team_id', $teamid)
							 -> where('cache', $code)
							 -> where('type', 'find')
							 -> get('points');
		 $res=$query->result();
		 if(count($res)>0) return true;
		 return false;

	}

	function cache_save($data,$code){
		$this->db->where('code', $code);
		$this->db->update('cache', $data);
	return	$this->points_set($data['owner'],$code,"save",$this->config->item("points_save_cache"));

	}


	//todo no es poden acumular
	function points_set($teamid,$code,$type,$points){


			$data = array(
							'team_id' => $teamid,
							'points'=>$points,
							'cache'=>strtoupper($code),
							'type'=>$type
			);

			try{
					$this->db->insert('points', $data);
				}catch(Exception $e){
					return "error";
			}
		if($points<0){
			return $points;
		}

		return "Heu obtingut ".$points." punts";
	}

	function points($teamid){
		$query = $this -> db
							 -> select('sum(points) as p')
							 -> where('team_id', $teamid)
							 ->get('points');

		 $res=$query->result();
		 if(count($res)>0){
			 return $res[0]->p;
		 }

	}

	function ranking(){
		$sql = "SELECT team.id,name,sum(points.points) as p FROM points,team where points.team_id=team.id group by team_id order by p desc";
		$query = $this->db->query($sql);
		 $res=$query->result();
		 return $res;

	}

	function get($team_id){

		$query = $this -> db
							 -> select('*')
							 -> where('id', $team_id)
							 -> get('team');

	  $res=$query->result();
		if(count($res)==0) return false;
		return $res[0];
  }


  function is_logined(){

  }

  function get_all(){
    $query = $this->db->get('team');
    return $query->result();
  }

}
