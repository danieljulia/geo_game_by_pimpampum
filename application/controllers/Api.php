<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}


	function index(){

	}

	function get_cache($code){
		$res=$this->cache->get_cache($code);
		print json_encode($res);

	}

	function check_code($code){
		$res['ok']=$this->cache->is_valid($code);
		print json_encode($res);

	}

	function caches($teamid=""){
		$caches=$this->cache->get_all($teamid);
		$found=array();
		if($this->config->item("mode")=="VIEW"){
			$found=$this->cache->get_found($teamid);

		}
   	print json_encode(array('caches'=>$caches,'found'=>$found));
	}

	function caches_found($teamid=""){
		$res=$this->cache->get_found($teamid);
		print json_encode($res);
	}

	function spoiler_seen($teamid,$code){
		$res=$this->team_model->points_set($teamid,$code,'spoiler_seen',$this->config->item("points_spoiler_seen"));
		print json_encode($res);
	}

	function image_seen($teamid,$code){
		$res=$this->team_model->points_set($teamid,$code,'image_seen',$this->config->item("points_image_seen"));
		print json_encode($res);
	}


	function images($code){

		$query = $this -> db
							 -> select('*')
							 -> where('cache', $code)
							 -> get('images');
		 $res=$query->result();
		 	print json_encode($res);
	}

	function track($team,$lat,$lng,$prec){

		$user_agent=$_SERVER['HTTP_USER_AGENT'];
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}


		$data = array(
        'team_id' => $team,
        'lat' => $lat,
        'lng' => $lng,
				'prec'=>$prec,
				'user_agent'=>$user_agent,
				'ip'=>$ip
		);

		print json_encode($data);
		$this->db->insert('tracking', $data);


	}

	function data($num,$id=0){

		$sql="SELECT *,tracking.id as tid FROM tracking,team where team.id=tracking.team_id and tracking.id>? order by updated asc limit ?";
		$query = $this->db->query($sql,array($id,intval($num)));
		 $res=$query->result();
		 print json_encode($res);
	}



}
