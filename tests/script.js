var base = {
  'Empty': L.tileLayer(''),
  'OpenStreetMap': L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    'attribution': 'Map data &copy; OpenStreetMap contributors'
  })
};



var map = L.map('map', {
  'center': [0, 0],
  'zoom': 0,
  'layers': [
    base.Empty
  ]
});

var geo_options = {
  enableHighAccuracy: true,
  maximumAge: 1000
    //timeout           : 1000
};

var control = L.control.layers(base).addTo(map);
var marker_created=false;
var current_marker,current_circle;

function onLocationFound(e) {
  console.log("he trobat la info ",e);
    var radius = e.accuracy / 2;

    if(!marker_created){
      current_marker=L.marker(e.latlng);
      current_circle=  L.circle(e.latlng, radius);

      current_marker.addTo(map)
        .bindPopup("You are within " + radius + " meters from this point").openPopup();

      current_circle.addTo(map);
        marker_created=true;
    }
}

function update(){
  map.locate({setView: true, maxZoom: 18});
}

map.on('locationfound', onLocationFound);

function onLocationError(e) {
    alert(e.message);
}

map.on('locationerror', onLocationError);


	map.locate({setView: true, maxZoom: 18});
