<html>
<head>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.2/css/uikit.gradient.min.css" integrity="sha256-TLgWPS4CoQk94wbnfeHKRaBpM3IQS42Y3pqb3CTDFJI=" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.2/css/components/progress.gradient.min.css" integrity="sha256-z+JFPUwyB++qqAA/9qdfL/hpFnwdDWTAq5gYt0+OkZE=" crossorigin="anonymous" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.2/css/components/form-file.gradient.min.css" integrity="sha256-E4mr2pSGSE3pbt5V6fEn+e2mA2xqfagllq4tRqJUsOk=" crossorigin="anonymous" />

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="js/exif.js"></script>
	<script type="text/javascript" src="js/ImageUploader.js"></script>
	<script type="text/javascript" src="js/custom.js"></script>
</head>
<body>
	<div class="uk-text-center uk-margin-top">
		<div class="uk-form-file">
			<button class="uk-button uk-button-success image-upload" id="upload-button-1"><i class="uk-icon-upload uk-margin-small-right"></i>Select and upload image</button>
			<input class="image-upload" data-id="1" id="uploadImage3" type="file" name="images[1]" accept="image/jpeg,image/jpg" data-product="1034">

			<div class="uk-alert uk-alert-success uk-hidden" id="upload-container-1">
				<p class="uk-text-center">Uploading, please wait...</p>
				<div id="upload-progress-1" class="uk-progress uk-progress-small uk-progress-success uk-progress-striped uk-active">
					<div class="uk-progress-bar"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="uk-text-center uk-margin-top">
		<div class="uk-form-file">
			<button class="uk-button uk-button-success image-upload" id="upload-button-2"><i class="uk-icon-upload uk-margin-small-right"></i>Select and upload image</button>
			<input class="image-upload" data-id="2" id="uploadImage2" type="file" name="images[2]" accept="image/jpeg,image/jpg" data-product="1034">

			<div class="uk-alert uk-alert-success uk-hidden" id="upload-container-2">
				<p class="uk-text-center">Uploading, please wait...</p>
				<div id="upload-progress-2" class="uk-progress uk-progress-small uk-progress-success uk-progress-striped uk-active">
					<div class="uk-progress-bar"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="uk-text-center uk-margin-top">
		<div class="uk-form-file">
			<button class="uk-button uk-button-success image-upload" id="upload-button-3"><i class="uk-icon-upload uk-margin-small-right"></i>Select and upload image</button>
			<input class="image-upload" data-id="3" id="uploadImage3" type="file" name="images[3]" accept="image/jpeg,image/jpg" data-product="1034">

			<div class="uk-alert uk-alert-success uk-hidden" id="upload-container-3">
				<p class="uk-text-center">Uploading, please wait...</p>
				<div id="upload-progress-3" class="uk-progress uk-progress-small uk-progress-success uk-progress-striped uk-active">
					<div class="uk-progress-bar"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
