<div class="wrap instruccions">
	<p><strong>Benvinguts i benvingudes a la Gimcana al Bosc!</strong> </p>
	<p>Per començar, selecciona el teu equip  <a href="<?php echo site_url("team/register")?>"> o crea'n un de nou</a>.</p>
<h2>Equips:</h2>
<ul id="llistat_equips">
  <?php foreach($teams as $team):?>
    <li><a href="<?php echo site_url("team/select/".$team->id)?>"><?php print $team->name?></a></li>
  <?php endforeach;?>
</ul>
<!--
<a class="tornar" href="<?php echo site_url()?>">Tornar al mapa</a>
-->
</div>
