<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Escape_Controller extends CI_Controller {


	public function __construct()
	{
		parent::__construct();
    //if not logined redirect to login
    //$this->session->set_flashdata('item', 'value');
  }

  public function validate(){




		$team_id=$this->session->userdata('team_id');


		if(!$team_id  || !$this->team_model->team_id_exists($team_id)){
			redirect('team/login');
		}
		$this->session->sess_expiration = 3600*48;
		$this->session->sess_expire_on_close = FALSE;
		$points=$this->team_model->points($team_id);

		$this->load->model("team_model");
		$team=$this->team_model->get($team_id);

		$team->caches=$this->cache->get_all($team_id);
		$team->found=$this->cache->get_found($team_id);
	  $team->total=$this->cache->get_total();

		if(!$team){
			redirect("team/login");
		}
		$team->points=$points;
    return $team;
  }

  public function load_view($view_name,$params=array()){

    $this->load->view("header");
    $this->load->view($view_name,$params);
    $this->load->view('footer');

  }


}
