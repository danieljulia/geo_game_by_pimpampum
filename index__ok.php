<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.css"  data-require="leaflet@0.7.3" data-semver="0.7.3" />
    <link rel="stylesheet" href="css/style.css" />
  </head>
  <body>
    <div class="control equip" class="info">
      Nom de l'equip
    </div>
    <button class="control deixar">Deixar aqui</button>
    <div  id="map"></div>


    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.js" data-require="leaflet@0.7.3" data-semver="0.7.3"></script>
    <script type="text/javascript" src="js/app.js"></script>
  </body>
</html>
