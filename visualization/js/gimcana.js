
var mygeo; //simple geo canvas


var tag_counter=1;
var lastroll=0;
var rollc=0;


$(document).ready(function(){


  mygeo=new simplegeocanvas("mycanvas");
  mygeo.background="#000";

  mygeo.init();
  mygeo.setCenter(41,2); //todo canviar centre

  mygeo.scale=50;

  mygeo.onDown=function(e){

  }

  //returns marker clicked
  mygeo.onClick=function(mrk){

  }
  mygeo.onUp=function(e){

  }
  mygeo.onMove=function(e){


  }
  mygeo.onPaint=function(e){


  }

  mygeo.onHashed=function(params){


  }


  mygeo.hashGet();

  mygeo.onRoll=function(r){


  }

getData(0);


/*
   $('#button1').click(function(){
    var tag=$('#tag1').val();
    if(tag=="") return;
    searchTag(tag);
   });


*/

  $("body").keyup(function(event){

      if(event.keyCode == 13){


      }
  });



   $('.zoomin').click(function(){
    mygeo.doScale(2);
   });
   $('.zoomout').click(function(){
    mygeo.doScale(0.5);
   });

    $('.center').click(function(){
    mygeo.centerLayers();
   });

   $('.toggle_tags').click(function(){
    $('.controls-tags').toggle();
    $('.ojito').toggleClass('hidden');
   });

});



function updateRoll(){
  if(lastroll.info!=undefined){
    $('.info').html(lastroll.info.photo.title._content);
  }
}


function getData(id){


  $.getJSON("../api/data/500/"+id,function(data){
    console.log(data.length);
    if(data.length==0) return;
    var i=0;
    $.each(data,function(i,val){

        var m=new sgmarker();
        m.init(val.lat,val.lng,val.name,val.prec,val.name)
        m.i=i;

        var canal=val.name;
        if(!mygeo.getLayer(canal)){
          var tag_color=get_random_color_ex();
          mygeo.addLayer(tag_color,canal);
          console.log("creat canal ",tag_color,val.name);
        }
        mygeo.addMarker(m,canal);

        i++;

    });

    var id=data[data.length-1].tid;
    if(id==undefined) return;
    console.log("seguint al ",id);
    setTimeout(function(){
      getData(id);
    },2000);

  });

}


var colors=new Array();

function color_get(){
  var ok=false;
  var j=0;
  while(!ok){
    var c=get_random_color_ex();
    if(!color_exists(c)){
      ok=true;
      colors.push(c);
    }
    j++;
    if(j==500){

      ok=true;
    }
  }
  return c;
}

function color_exists(c){
  for(var i=0;i<colors.length;i++){
    if(colors[i]==c) return true;
  }
  return false;
}
