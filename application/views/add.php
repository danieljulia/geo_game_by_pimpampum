
<?php
/*
<form action="<?php echo site_url()?>/main/saveCache" method="post">
  */

  ?>

<?php echo form_open_multipart('main/saveCache');?>

    <div class="notification hidden" id="form-msg"></div>
    <form id="myform">

      <label class="label" for="code">Codi</label><strong>Sobretot deixeu el catxé on l'heu trobat</strong>
      <input class="input" type="text" name="code" placeholder="introdueix el codi de l''objecte">


<div class="form-options hidden">
  <label class="mode-rec label label_info" for="msg">Deixa un missatge "spoiler" (instruccions per si no ho troben)
  <textarea class="mode textarea" name="msg"></textarea>
 </label>

<!--
   <p class="mode-view">Enhorabona! Has obtingut <?php echo $this->config->item("points_cache")?> punts!
   </p>
-->

<!--
   <input class="button mode-view" type="submit" value="Confirma!">-->
</div>
  <input id="seguent" class="button" type="submit" value="Següent">
<input type="hidden" name="team_id" value="<?php echo $team->id?>">
<input type="hidden" name="lat" value="<?php echo $lat?>">
<input type="hidden" name="lng" value="<?php echo $lng?>">
<input type="hidden" name="precision" value="<?php echo $precision?>">


</form>

  </div>



</form>
<script>

var code_validated=false;

$(function() {
  $('input[name="code"]').on('input',function(ev){
    var code=$(this).val();
    $.getJSON( "<?php echo site_url()?>/api/check_code/"+code, function( data ) {

      /*
      if(data.ok==true){
        $('.form-options').removeClass("hidden");
      }else{
        $('.form-options').addClass("hidden");
      }
      */
    });
  });

  function code_validate(code){
    $.getJSON( "<?php echo site_url()?>/api/get_cache/"+code, function( data ) {

      console.log(data);
      if(data==null){
          showMsg("El codi no és vàlid");
      }else{


        if(mode=="VIEW"){
          $('input[name="code"]').hide();
          $('label[for="code"]').append(" "+code);

          $('#seguent').attr("value","Aconseguir <?php echo $this->config->item('points_cache')?> punts");
          code_validated=true;

        }else{
          if(data.owner=="0"){
            $('input[name="code"]').hide();
            $('label[for="code"]').append(" "+code);
           showMsg("Molt bé, el codi és correcte");

           $('.form-options').removeClass("hidden");
           code_validated=true;
          }else{
           showMsg("Aquest objecte ja està guardat");
          }

        }


      }




    });
  }

  $('#seguent').on('click',function(ev){
    if(!code_validated){
      code_validate( $('input[name="code"]').val() );
      ev.preventDefault();
    }else{
        if(mode=="REC"){
          if($('textarea[name="msg"]').val()==""){
            showMsg("Si us plau, deixa un missatge 'spoiler'");
            ev.preventDefault();
          }
        }
    }



  });

});



</script>
