<div class="page">
	<p class="info">
		<?php if($this->config->item('mode')=="REC"):?>
			Estem a la fase d'amagar catxés: busqueu un lloc ben bonic que vulgueu compartir i deixeu-hi l'etiqueta. Cal que el registreu a la web perquè posteriorment d'altres equips el puguin descobrir.
		<?php else: ?>
			Estem a la fase de descoberta. Acosteu-vos a qualsevol dels punts que estan al mapa, trobeu el catxé i registreu-lo a la web per indicar que l'heu trobat.
		<?php endif;?>
		<br><a class="tornar" href="<?php echo site_url()?>">Tornar al mapa</a>
	</p>
<h1>Rànquing</h1>
	<ul class="ranking">


	<?php foreach($ranking as $t):?>

	<li <?php if($t->id==$team->id) echo "class='selected'"?>><span class="ranking_nom"><?php echo $t->name?></span><span class="punts"><?php echo $t->p?> punts</span> </li>
	<?php endforeach ?>
	</ul>

	<h1>Com obtenir punts</h1>
	<ul class="ranking punts">
	<li>Per deixar un objecte <span class="punts"><?php echo $this->config->item('points_save_cache')?> punts</span></li>
	<li>Per trobar un objecte d'un altre equip<span class="punts"><?php echo $this->config->item('points_cache')?> punts</span></li>
	<li>Per veure el missatge spoiler (només la primera vegada)<span class="punts"><?php echo $this->config->item('points_spoiler_seen')?> punts</span></li>
	<li>Per veure l'imatge  spoiler (només la primera vegada<span class="punts"><?php echo $this->config->item('points_image_seen')?> punts</span></li>
</ul>




</div>
