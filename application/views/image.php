 <div class="wrap">
 	<div class="notification hidden" id="form-msg"></div>
 </div>

	<form id="myform" action="<?php echo site_url('main/cache_saved')?>">
	    <p>Fes una foto o selecciona una imatge de la galeria per documentar aquest lloc</p>
	    <div class="input_pujar"><input class="image-upload"  data-code="<?php echo $code?>" data-teamid="<?php echo $team->id?>" id="uploadImage" type="file" name="image" accept="*">
	    </div>
		<input id="seguent" class="button" type="submit" value="Següent">
	</form>

    <div class="alert alert-success hidden wrap" id="upload-container">
      <p class="text-center">Pujant, si us plau espera..</p>
      <p id="uploading-msg"></p>

      <div id="upload-progress" class="progress progress-small progress-success progress-striped active">
        <div class="progress-bar"></div>
      </div>
    </div>
