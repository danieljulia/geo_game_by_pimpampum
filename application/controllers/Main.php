<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require "Escape.php";

class Main extends Escape_Controller {


	public function __construct()
	{
		parent::__construct();


	}


	function index(){
		$data=array();
		$team=$data['team']=$this->validate();


		$this->load->view('header',$data);
		$this->load->view('map',$data);
		$this->load->view('footer');
	}

	function test(){
		$t=$this->cache->get_total();
		print $t;
		exit();
		$this->team_model->ranking();
		exit();
		$p=$this->team_model->points(1);
		print $p;
		exit();
		$code="WYKFL";
		if(!$this->cache->is_saved($code)){
			print "guardant cache";
			$this->cache->save($code,27,27,1,27,"el meu missatge");
		}else{
			print "Ja esta guardat";

		}

	}

  function addCache(){
		$data=array();
		$data['team']=$this->validate();

		/*
		if($this->input->post("msg")){
			print "hi ha missatge";
		}else{
			print "has de deixar un missatge";
		}
		*/

		if($this->config->item("mode")=="REC"){
			if($this->input->post("lat")==""){
				$this->session->set_flashdata('msg', 'Si us plau activa la localització per poder guardar');
				redirect('');
			}
		}


		$data['team_id']=$this->input->post("team_id");
		$data['lat']=$this->input->post("lat");
		$data['lng']=$this->input->post("lng");
		$data['precision']=$this->input->post("precision");

		$this->load->view('header',$data);
		$this->load->view('add',$data);
		$this->load->view('footer');
	}

	function saveCache(){

		$code=$this->input->post("code");


		if($this->config->item("mode")=="VIEW"){
			//torna a la home , i registra punts si cal
			$msg=$this->team_model->cache_find($this->input->post("team_id"),$code,"find");
			$this->session->set_flashdata('msg', $msg);
			redirect('');
			return;
		}else{
				//si ja està guardat no et deixa
				if($this->team_model->cache_saved($code)){
					$this->session->set_flashdata('msg', "Aquest catxé ja està guardat!");
					redirect('');
					return;
				}
		}
		//todo veriticar que les dades son correctes
		$data = array(
						'owner' => $this->input->post("team_id"),
		        'prec' => $this->input->post("precision"),
		        'lat' => $this->input->post("lat"),
		        'lng' => $this->input->post("lng"),
						'msg' => $this->input->post("msg"),
						'saved' => date('Y-m-d H:i:s')
		);

		$punts=$this->team_model->cache_save($data,$code);


		$this->session->set_flashdata('msg', 'El catxé ha quedat guardat correctament.');
		redirect('main/addImage/'.$this->input->post("code"));

	}

		function addImage($code){
				$data=array();
				$data['team']=$this->validate();
				$data['code']=$code;
				$data['uploading']=true;

				$this->load->view('header',$data);
				$this->load->view('image',$data);
				$this->load->view('footer');
		}

		function sendImage(){
			//Main code, used for processing uploaded image
			$teamid = (int)$this->_getValue('teamid');
			$code = $this->_getValue('code');

			$raw_64 = file_get_contents('php://input');
			// Base64 decode the input stream
			$img = base64_decode($raw_64);
			if ($this->_saveTempImage($teamid, $code,$img))
				die('ok');
			else
				die('error');

		}

		function cache_saved(){
			$punts=$this->config->item("points_save_cache");
			$this->session->set_flashdata('msg', 'Molt bé, heu obtingut '.$punts." punts");
			redirect('');

		}

		function ranking(){

			$data=array();
			$data['team']=$this->validate();
			$data['ranking']=$this->team_model->ranking();


			$this->load->view('header',$data);
			$this->load->view('ranking',$data);
			$this->load->view('footer');
		}

		function help(){
			$this->load->view('header');
			$this->load->view('help');
			$this->load->view('footer');
		}

		function _getValue($key, $defaultValue = false)
		{
		 	if (!is_string($key))
				return false;
			$ret = (isset($_POST[$key]) ? $_POST[$key] : (isset($_GET[$key]) ? $_GET[$key] : $defaultValue));

			if (is_string($ret))
				$ret = stripslashes(urldecode(preg_replace('/((\%5C0+)|(\%00+))/i', '', urlencode($ret))));
			else
				return false;
			return $ret;
		}

		//Helper function to save image
		function _saveTempImage($teamid, $code,$contents){
			//You can use your imagination and store the images on the server using your own naming logic, we use the following:
			//todo registrar aquesta foto a la taula corresponent


			$f='team'.$teamid.'_'.$code.'_'.rand(1,999999).'.jpg';
			$this->cache->set_image($code,$f,$teamid,"spoiler");

			$filename = FCPATH.'uploads/'.$f;
			$res = file_put_contents($filename, $contents);
			if ($res===false)
				return false;

			//Validate that it's a JPEG image
			$im = @imagecreatefromjpeg($filename);
			if ($im)
				return true;
			//Delete the image if it's not JPEG
			@unlink($filename);
			return false;
		}


}
