<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/leaflet/0.7.3/leaflet.css"  data-require="leaflet@0.7.3" data-semver="0.7.3" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bulma/0.7.0/css/bulma.css">
	<link href="https://fonts.googleapis.com/css?family=IBM+Plex+Sans+Condensed:400,700" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.min.css"/>

    <?php if(isset($uploading)):?>


    <?php
    /*
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.2/css/uikit.gradient.min.css" integrity="sha256-TLgWPS4CoQk94wbnfeHKRaBpM3IQS42Y3pqb3CTDFJI=" crossorigin="anonymous" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.2/css/components/progress.gradient.min.css" integrity="sha256-z+JFPUwyB++qqAA/9qdfL/hpFnwdDWTAq5gYt0+OkZE=" crossorigin="anonymous" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.27.2/css/components/form-file.gradient.min.css" integrity="sha256-E4mr2pSGSE3pbt5V6fEn+e2mA2xqfagllq4tRqJUsOk=" crossorigin="anonymous" />

*/
?>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    	<script type="text/javascript" src="<?php print base_url()?>js/exif.js"></script>
    	<script type="text/javascript" src="<?php print base_url()?>js/ImageUploader.js"></script>
    	<script type="text/javascript" src="<?php print base_url()?>js/custom.js"></script>


    <?php endif;?>




    <link rel="stylesheet" href="<?php print base_url()?>/css/style.css" />
    <style>
    <?php if($this->config->item("mode")=="REC"):?>
      .mode-view{
        display:none;
      }
    <?php else: ?>
    .mode-rec{
      display:none;
    }
    <?php endif;?>
  </style>

    <script
      src="https://code.jquery.com/jquery-2.2.4.min.js"
      integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
      crossorigin="anonymous"></script>

<script>
<?php if(isset($team)):?>
var team_id=<?php echo $team->id?>;
var mode="<?php echo $this->config->item("mode")?>";
<?php endif;?>
</script>

  </head>
  <div class="container">
    <header class="wrap">
         <h1 class=""><a href="<?php echo site_url()?>">
           <?php echo $this->lang->line('app_name');?>
           </a></h1>
          <!-- <li class="navbar-item" id="geo"></li> -->

           <?php if(isset($team)):?>

     <!--  <h2 class="navbar-item" ></h2> -->
    <?php endif;?>

         <span class="navbar-burger burger" data-target="navbarMenuHeroC">
           <span></span>
           <span></span>
           <span></span>
         </span>
       </header>


	   <div id="navbarMenuHeroC" class="navbar-menu">
            <ul class="navbar-end"> <!-- is-active -->
            <li>  <a class="navbar-item  tornar" href="<?php echo site_url()?>">Mapa</a></li>
                  <li><a class="navbar-item " href="<?php echo site_url("team/login")?>">Canviar d'equip</a></li>
			            <li><a href="<?php echo site_url("main/ranking")?>" class="navbar-item">Rànquing</a></li>
                  <li><a href="<?php echo site_url("main/help")?>" class="navbar-item">Ajuda</a></li>
            </ul>
          </div>

<ul class="resum wrap">
  <?php if(isset($team)):?>
	<li class="nom_equip"><?php echo $team->name?></li>
  <li class="info">
<?php if($this->config->item("mode")=="REC"): ?>
    <?php print count($team->caches) ?> catxés amagats <span class="total">(<?php echo $team->total ?> en total)</span>
<?php else: ?>
    <?php print count($team->found) ?> / <?php print count($team->caches) ?> catxés trobats <span class="total">(<?php echo $team->total ?> en total)</span>
<?php endif; ?>

  </li>
	<li class="points"><span class="counter"><?php echo $team->points?></span> punts</li>
  <?php endif?>
</ul>

<?php
$msg=$this->session->flashdata('msg');
if($msg!=""):
  ?>
<div class="wrap">
  <div class="notification">
    <?php print $msg?>
  </div>
</div>
<?php endif;?>
