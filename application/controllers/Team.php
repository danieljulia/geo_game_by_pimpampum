<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require "Escape.php";

class Team extends Escape_Controller {


	public function __construct()
	{
		parent::__construct();

	}


	function index(){
      print "index";
	}

	function login(){
    $data=array();
    $data['teams']=$this->team_model->get_all();
    $this->load_view("team/login",$data);
  }

	function select($team_id){
		$this->session->set_userdata('team_id',$team_id);
		redirect('');
	}


  function register(){
		if($this->input->post("team")){
			$id=$this->team_model->register($this->input->post("team"));
			if($id==-1){
				$this->session->set_flashdata('msg', "Aquest nom d'equip ja existeix");
				redirect('team/register');
			}
			$this->select($id);
			$this->session->set_flashdata('msg', 'El nom del teu equip ha quedat registrat i ja estàs participant!');
			redirect('');
		}
    $this->load_view("team/register");
  }


}
