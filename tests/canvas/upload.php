<?php
//Helper function returns value from GET or POST request
function getValue($key, $defaultValue = false)
{
 	if (!is_string($key))
		return false;
	$ret = (isset($_POST[$key]) ? $_POST[$key] : (isset($_GET[$key]) ? $_GET[$key] : $defaultValue));

	if (is_string($ret))
		$ret = stripslashes(urldecode(preg_replace('/((\%5C0+)|(\%00+))/i', '', urlencode($ret))));
	else
		return false;
	return $ret;
}

//Helper function to save image
function saveTempImage($id_product, $id_image, $contents){
	//You can use your imagination and store the images on the server using your own naming logic, we use the following:
	$filename = __DIR__.'/uploads/product-'.$id_product.'-'.$id_image.'.jpg';
	$res = file_put_contents($filename, $contents);
	if ($res===false)
		return false;

	//Validate that it's a JPEG image
	$im = @imagecreatefromjpeg($filename);
	if ($im)
		return true;
	//Delete the image if it's not JPEG
	@unlink($filename);
	return false;
}

//Main code, used for processing uploaded image
$id_product = (int)getValue('id_product');
$id_image = (int)getValue('id_image');
$raw_64 = file_get_contents('php://input');
// Base64 decode the input stream
$img = base64_decode($raw_64);
if (saveTempImage($id_product, $id_image, $img))
	die('ok');
else
	die('error');
?>
