<div class="container">

<p><strong>Us proposem una activitat per descobrir l’entorn de La Plana.</strong> </p>
<p>Aquells que ja hi heu estat, podeu compartir els vostres racons favorits. Per aquells que és la primera vegada, us animen a voltar, explorar i deixar-vos guiar pels indrets proposats.
</p><p>Aquesta activitat té dues fases: una d’amagada, en que deixarem els objectes (catxés) i els registrarem a la web, i una de descoberta, en que buscarem els objectes que hi haurà distribuïts per l’entorn.
</p>
<a class="anar-mapa" href="<?php echo site_url()?>">Anar al mapa</a>
</div>
